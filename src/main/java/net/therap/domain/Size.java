package net.therap.domain;

import java.lang.annotation.*;

/**
 * @author rakibul.hasan
 * @since 2/5/20
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Size {

    int min() default 1;

    int max() default 100;

    String message() default "Length must be {min}-{max}";
}
