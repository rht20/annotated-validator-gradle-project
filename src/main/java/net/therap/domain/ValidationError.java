package net.therap.domain;

/**
 * @author rakibul.hasan
 * @since 2/5/20
 */
public class ValidationError {

    private String errorMessage;

    public ValidationError(String error) {
        setErrorMessage(error);
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
